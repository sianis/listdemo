package org.sianis.listdemo;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends Activity implements WearableListView.ClickListener {

    private WearableListView mWearableListView;
    private List<ApplicationInfo> installedApplications = new ArrayList<>();
    private PackageManager packageManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        packageManager = getPackageManager();
        mWearableListView = (WearableListView) findViewById(R.id.list);
        installedApplications = getInstalledApplications();
        mWearableListView.setAdapter(new Adapter(this));
        mWearableListView.setClickListener(this);
    }

    private List<ApplicationInfo> getInstalledApplications() {
        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(PackageManager.GET_ACTIVITIES);
        List<ApplicationInfo> filtered = new ArrayList<>();
        for (ApplicationInfo applicationInfo : installedApplications) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(applicationInfo.packageName)) {
                    filtered.add(applicationInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Collections.sort(filtered, new Comparator<ApplicationInfo>() {
            @Override
            public int compare(ApplicationInfo lhs, ApplicationInfo rhs) {
                String lhsLabel = lhs.loadLabel(packageManager).toString();
                String rhsLabel = rhs.loadLabel(packageManager).toString();
                return lhsLabel.compareToIgnoreCase(rhsLabel);
            }
        });
        return filtered;
    }

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        //Start activity
        startActivity(packageManager.getLaunchIntentForPackage(installedApplications.get(viewHolder.getPosition()).packageName));
    }

    @Override
    public void onTopEmptyRegionClick() {
        //Do nothing
    }

    private final class Adapter extends WearableListView.Adapter {
        private final Context mContext;
        private final LayoutInflater mInflater;

        private Adapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new WearableListView.ViewHolder(
                    mInflater.inflate(R.layout.list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
            ImageView icon = (ImageView) holder.itemView.findViewById(R.id.icon);
            TextView view = (TextView) holder.itemView.findViewById(R.id.name);
            view.setText(packageManager.getApplicationLabel(installedApplications.get(position)));
            icon.setImageDrawable(installedApplications.get(position).loadIcon(packageManager));
            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return installedApplications.size();
        }
    }
}
